<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::group(['namespace' => 'Web', 'prefix' => 'web'], function(){

    Route::get('index', ['as' => 'index', 'uses' => 'ImportController@index']);
    Route::post('import', ['as' => 'import', 'uses' => 'ImportController@import']);

});