## Importing Excel File in Mysql Using PHPSpreadsheets and Laravel Framework

Simple example for importing excel xlsx file into mysql using Laravel Framework and PHPSpreadsheets library.

## Installation :
* Clone project.
* composer install.
* create empy database and update .env file with corresponding db information and change mail driver to log and queue to db.
* Run php artisan key:generate
* Run php artisan migrate
* php artisan serve
* Run worker : php artisan queue:work
* Go to http://127.0.0.1:8000/web/index

## Notes:
* The accepted file is only "xlsx" extension.
* There is an example for the excelsheet in the root directory.
* Fields are name,description,price
* All Fields are mandatory.
* If a cell is empty so the the record is not accepted.
* After finishing the importing job, an email is sent (in Log) with the imported records number and failed records number.