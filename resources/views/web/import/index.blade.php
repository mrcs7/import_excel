@if (count($errors) > 0)
    <div class="error">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<form action="{{route('import')}}" enctype="multipart/form-data" method="post">
    <div class="form-group">
        <label for="excel">Excel: </label>
        <input type="file" class="form-control" name="imported_file"/>
    </div>
    {{csrf_field()}}
    <div class="form-group">
        <input type="submit" class="form-control"/>
    </div>
</form>