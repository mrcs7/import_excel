<?php
/**
 * Created by PhpStorm.
 * User: iSlAm
 * Date: 10/15/2018
 * Time: 2:32 AM
 */

namespace App\Services;


use App\ImportLogs;
use App\Products;

class ProductImporter
{
    protected $highestRow;
    protected $num_of_accepted;
    protected $num_of_refused;

    public function product_import($excel_file){
        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        $spreadsheet = $reader->load($excel_file);
        $worksheet = $spreadsheet->getActiveSheet();

        // Get the highest row and column numbers referenced in the worksheet
        $this->highestRow = $worksheet->getHighestRow(); // e.g. 10
        $this->num_of_accepted = 0;
        $this->num_of_refused = 0 ;
        for ($row = 2; $row <=  $this->highestRow; ++$row) {
            $name =null;
            $desc=null;
            $price=null;
            for ($col = 1; $col <= 3; ++$col) {
                $value = $worksheet->getCellByColumnAndRow($col, $row)->getValue();
                switch ($col){
                    case 1:
                        $name= $value;
                        break;
                    case 2:
                        $desc = $value;
                        break;
                    case 3:
                        $price = $value;
                        break;
                    default:
                        break;
                }

            }
            if($name==null || $desc ==null || $price==null){
                $this->num_of_refused++;
            }else{
                $product = $this->store_product($name,$desc,$price);
                if($product){
                    $this->num_of_accepted++;
                }
            }

        }

        $this->log($excel_file,$this->num_of_accepted,$this->num_of_refused);

        return[
            "imported"=>$this->num_of_accepted,
            "failed"=>$this->num_of_refused
        ];

    }

    public function store_product($name,$desc,$price){
        $product = new Products();
        $product->name = $name;
        $product->description = $desc;
        $product->price = $price;
        if($product->save()){
            return true;
        }
    }

    public function log($file_name,$imported,$failed){
        $log = new ImportLogs();
        $log->file = $file_name;
        $log->num_success = $imported;
        $log->num_fail = $failed;
        $log->save();
        return true;

    }

}