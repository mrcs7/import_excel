<?php

namespace App\Jobs;

use App\ImportLogs;
use App\Mail\FinishedImportMail;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class FinishedImportJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $excel_sheet;
    public function __construct($excel_sheet)
    {
        //
        $this->excel_sheet = $excel_sheet;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //Get The Right Log Records
        $log = ImportLogs::where('file', $this->excel_sheet)->first();
        Log::debug('An informational message.');

            $import['success']= $log->num_success;
            $import['failed'] = $log->num_fail;
            Log::debug($import['success']);
            Mail::to("test@example.com")
                ->send(new FinishedImportMail($import));

            Log::debug('Mail Failed');


    }
}
