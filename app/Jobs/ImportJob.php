<?php

namespace App\Jobs;

use App\Services\ProductImporter;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ImportJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $excel_file;
    public function __construct($excel_file)
    {
        //
        $this->excel_file = $excel_file;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ProductImporter $productImporter)
    {
        //
        $import = $productImporter->product_import($this->excel_file);
    }
}
