<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImportLogs extends Model
{
    //
    protected $table = 'import_logs';
}
