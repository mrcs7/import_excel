<?php
/**
 * Created by PhpStorm.
 * User: iSlAm
 * Date: 10/14/2018
 * Time: 11:34 PM
 */

namespace App\Http\Controllers\Web;


use App\Http\Controllers\Controller;
use App\Http\Requests\Import;
use App\Jobs\FinishedImportJob;
use App\Jobs\ImportJob;
use App\Products;
use App\Services\ProductImporter;

class ImportController extends Controller
{
    protected $product_importer;
    public function __construct()
    {
        $this->product_importer = new ProductImporter();
    }

    public function index(){
        return view('web.import.index', []);

    }

    public function import(Import $request){
        $file = $request->file('imported_file');
        $stored_path = $file->store('files', 'public');
        $file_link = public_path('storage/'.$stored_path);
//        $store_in_db = $this->product_importer->product_import($file_link);
//        dd($store_in_db);
        ImportJob::withChain([
            new FinishedImportJob($file_link)
        ])->dispatch($file_link);
        return redirect()->route('index');

    }


}