<?php
/**
 * Created by PhpStorm.
 * User: iSlAm
 * Date: 10/15/2018
 * Time: 12:59 AM
 */

namespace App\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;

class Import extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'imported_file' => 'required|mimes:xlsx'
        ];
    }
}